FROM node:lts-alpine

WORKDIR /app

COPY package*.json /app
RUN npm install
RUN npm install react-scripts

COPY . /app/

RUN npm run build

EXPOSE 3000
CMD ["npm", "run", "start"]