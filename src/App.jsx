import React from "react";
import Sidebar from "./components/sidebar/Sidebar";
import Topbar from "./components/topbar/Topbar";
import "./App.css";
import Home from "./pages/home/Home";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import CustomerList from "./pages/userList/CustomerList";
import SellerList from "./pages/userList/SellerList";
import Customer from "./pages/user/Customer";
import Seller from "./pages/user/Seller";
import NewCustomer from "./pages/newUser/NewCustomer";
import NewSeller from "./pages/newUser/NewSeller";
import ProductList from "./pages/productList/ProductList";
import Product from "./pages/product/Product";
import NewProduct from "./pages/newProduct/NewProduct";
import Login from "./pages/login/Login";
import Register from "./pages/register/Register";

import { useSelector } from "react-redux";

function App() {
  const admin = useSelector((state) => state.user.currentUser);
  // useSelector((state) => state.user.currentUser.is_admin);

  return (
    <Router>
      <Route path="/login">{admin ? <Redirect to="/" /> : <Login />}</Route>
      <Route path="/register">{admin ? <Redirect to="/" /> : <Register />}</Route>

      {admin && (
        <>
          <Topbar />
          <div className="container">
            <Sidebar />
            <Route exact path="/">
              <Home />
            </Route>

            <Route path="/customers">
              <CustomerList />
            </Route>
            <Route path="/customers/:uuid">
              <Customer />
            </Route>
            <Route path="/newCustomer">
              <NewCustomer />
            </Route>

            <Route path="/sellers">
              <SellerList />
            </Route>
            <Route path="/sellers/:uuid">
              <Seller />
            </Route>
            <Route path="/newSeller">
              <NewSeller />
            </Route>

            <Route path="/products">
              <ProductList />
            </Route>
            <Route path="/products/:productId">
              <Product />
            </Route>
            <Route path="/newproduct">
              <NewProduct />
            </Route>
          </div>
        </>
      )}
    </Router>
  );
}

export default App;
