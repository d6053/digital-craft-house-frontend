import React from "react";
import { useDispatch } from "react-redux";
import { logoutFunc } from "../../redux/sellerApiCalls";

import "./topbar.css";

export default function Topbar() {
  const root = JSON.parse(localStorage.getItem("persist:root"));
  const currentUser = JSON.parse(root.user).currentUser;

  const dispatch = useDispatch();

  const handleClick = (e) => {
    // e.preventDefault();
    logoutFunc(dispatch);
  };

  return (
    <div className="topbar">
      <div className="topbarWrapper">
        <div className="topLeft">
          <span className="logo">Digital-Craft-House-Admin</span>
        </div>
        <div className="topRight">
          <h3>{currentUser && currentUser.email ? currentUser.email:""}</h3>
          <img
            src="?auto=compress&cs=tinysrgb&dpr=2&w=500"
            alt=""
            className="topAvatar"
          />
          <button onClick={handleClick}>Log out</button>
        </div>
      </div>
    </div>
  );
}
