import React from "react";
import "./widgetSm.css";
import { Visibility } from "@material-ui/icons";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { userRequest} from "../../requestMethods";


export default function WidgetCustomers() {
  const [customers, setCustomers] = useState([]);

  useEffect(() => {
    const getCustomers = async () => {
      try {
        const res = await userRequest.get("/customers",{
          withCredentials : true
        } );
        setCustomers(res.data);
      } catch {}
    };
    getCustomers();
  }, []);

  return (
    <div className="widgetSm">
      <span className="widgetSmTitle">Customers</span>
      <ul className="widgetSmList">
        {customers.map((customer) => (
          <li className="widgetSmListItem" key={customer.uuid}>
            <img
              src={
                customer.img ||
                "https://crowd-literature.eu/wp-content/uploads/2015/01/no-avatar.gif"
              }
              alt=""
              className="widgetSmImg"
            />
            <div className="widgetSmCustomer">
              <span className="widgetSmUsername">
                {customer.first_name} {customer.last_name}
              </span>
            </div>
            <Link to={"/customers/" + customer.uuid}>
              <button className="widgetSmButton">
                <Visibility className="widgetSmIcon" />
                Display
              </button>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
}
