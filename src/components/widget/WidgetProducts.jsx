import React from "react";
import "./widgetSm.css";
import { Visibility } from "@material-ui/icons";
import { useEffect, useState } from "react";
import { productRequest} from "../../requestMethods";
import { Link } from "react-router-dom";

export default function WidgetProducts() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    const getProducts = async () => {
      try {
        const res = await productRequest.get("/products");
        setProducts(res.data);
      } catch {}
    };
    getProducts();
  }, []);

  return (
    <div className="widgetSm">
      <span className="widgetSmTitle">Products</span>
      <ul className="widgetSmList">
        {products.map((product) => (
          <li className="widgetSmListItem" key={product.uuid}>
            <img
              src={
                product.img ||
                "https://crowd-literature.eu/wp-content/uploads/2015/01/no-avatar.gif"
              }
              alt=""
              className="widgetSmImg"
            />
            <div className="widgetSmProduct">
              <span className="widgetSmUsername">{product.title}</span>
            </div>
            <Link to={"/products/" + product.uuid}>
              <button className="widgetSmButton">
                <Visibility className="widgetSmIcon" />
                Display
              </button>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
}
