import React from "react";
import "./widgetSm.css";
import { Visibility } from "@material-ui/icons";
import { useEffect, useState } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { userRequest} from "../../requestMethods";


export default function WidgetSeller() {
  const [sellers, setSellers] = useState([]);

  useEffect(() => {
    const getSellers = async () => {
      try {
        const res = await userRequest.get("/sellers",{
          withCredentials : true
        });
        setSellers(res.data);
      } catch {}
    };
    getSellers();
  }, []);

  return (
    <div className="widgetSm">
      <span className="widgetSmTitle">Sellers</span>
      <ul className="widgetSmList">
        {sellers.map((seller) => (
          <li className="widgetSmListItem" key={seller.uuid}>
            <img
              src={
                seller.img ||
                "https://crowd-literature.eu/wp-content/uploads/2015/01/no-avatar.gif"
              }
              alt=""
              className="widgetSmImg"
            />
            <div className="widgetSmCustomer">
              <span className="widgetSmUsername">
                {seller.first_name} {seller.last_name}
              </span>
            </div>
            <Link to={"/sellers/" + seller.uuid}>
              <button className="widgetSmButton">
                <Visibility className="widgetSmIcon" />
                Display
              </button>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
}
