import React from "react";
import "./home.css";
import WidgetCustomers from "../../components/widget/WidgetCustomers";
import WidgetSellers from "../../components/widget/WidgetSellers";
import WidgetProducts from "../../components/widget/WidgetProducts";

export default function Home() {
  return (
    <div className="home">
      <div className="homeWidgets">
        <WidgetCustomers />
        <WidgetSellers />
        <WidgetProducts />
      </div>
    </div>
  );
}
