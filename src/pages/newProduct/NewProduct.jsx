import React from "react";
import "./newProduct.css";
import { useState } from "react";
import { addProduct } from "../../redux/productApiCalls";
import { useDispatch } from "react-redux";

export default function NewProduct() {
  const [setInputs] = useState({});
  const [setFile] = useState(null);
  const [setCat] = useState([]);
  const dispatch = useDispatch();

  const [formValue, setFormValue] = useState({
    title: "",
    description: "",
    price: 0,
    categories: "",
  });

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormValue((prevState) => {
      return {
        ...prevState,
        [name]: value,
      };
    });
  };

  const handleChangePrice = (event) => {
    formValue.price = parseFloat(event.target.value);
  };

  const handleCategory = (e) => {
    formValue.categories = e.target.value.split(",");
  };

  const handleClick = (e) => {
    e.preventDefault();
    addProduct(formValue, dispatch);
    window.alert("New product is created");
  };

  return (
    <div className="newProduct">
      <h1 className="addProductTitle">New Product</h1>
      <form className="addProductForm">
        <div className="addProductItem">
          <label>Image</label>
          <input
            type="file"
            id="file"
            onChange={(e) => setFile(e.target.files[0])}
          />
        </div>
        <div className="addProductItem">
          <label>Title</label>
          <input name="title" type="text" onChange={handleChange} />
        </div>
        <div className="addProductItem">
          <label>Description</label>
          <input name="description" type="text" onChange={handleChange} />
        </div>
        <div className="addProductItem">
          <label>Price</label>
          <input name="price" type="number" onChange={handleChangePrice} />
        </div>
        <div className="addProductItem">
          <label>Categories</label>
          <input name="categories" type="text" onChange={handleCategory} />
        </div>

        <button onClick={handleClick} className="addProductButton">
          Create
        </button>
      </form>
    </div>
  );
}
