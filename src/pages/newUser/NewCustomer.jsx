import React from "react";
import "./newUser.css";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { addCustomer } from "../../redux/customerApiCalls";

export default function NewCustomer() {
  const [inputs, setInputs] = useState({});
  const [setFile] = useState(null);
  const dispatch = useDispatch();

  const [formValue, setFormValue] = useState({
    email: "",
    first_name: "",
    last_name: "",
    password: "",
    is_admin: 0,
  });

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormValue((prevState) => {
      return {
        ...prevState,
        [name]: value,
      };
    });
  };

  const handleClick = (e) => {
    e.preventDefault();
    addCustomer(formValue, dispatch);
    window.alert("New customer is created");
  };

  return (
    <div className="newUser">
      <h1 className="newUserTitle">New Customer</h1>
      <form className="newUserForm">
        <div className="addUserItem">
          <label>Image</label>
          <input
            type="file"
            id="file"
            onChange={(e) => setFile(e.target.files[0])}
          />
        </div>
        <div className="newUserItem">
          <label>Email</label>
          <input name="email" type="email" onChange={handleChange} />
        </div>
        <div className="newUserItem">
          <label>First Name</label>
          <input name="first_name" type="text" onChange={handleChange} />
        </div>
        <div className="newUserItem">
          <label>Last Name</label>
          <input name="last_name" type="text" onChange={handleChange} />
        </div>
        <div className="newUserItem">
          <label>Password</label>
          <input name="password" type="password" onChange={handleChange} />
        </div>

        <button onClick={handleClick} className="newUserButton">
          Create
        </button>
      </form>
    </div>
  );
}
