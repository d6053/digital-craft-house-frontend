import React from "react";
import { Link, useLocation } from "react-router-dom";
import "./product.css";
import { Publish } from "@material-ui/icons";
import { useSelector, useDispatch } from "react-redux";
import { useState } from "react";
import { updateProduct } from "../../redux/productApiCalls";

export default function Product() {
  const location = useLocation();
  const productUuid = location.pathname.split("/")[2];
  const dispatch = useDispatch();

  const products = useSelector((state) => state.product.products);
  var product = products.find((p) => String(p.uuid) === productUuid);

  const [formValue, setFormValue] = useState({
    uuid: product.uuid,
    title: product.title,
    description: product.description,
    price: product.price,
    categories: product.categories,
  });

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormValue((prevState) => {
      return {
        ...prevState,
        [name]: value,
      };
    });
  };

  const handleChangePrice = (event) => {
    formValue.price = parseFloat(event.target.value);
  };

  const handleCategory = (e) => {
    formValue.categories = e.target.value.split(",");
  };

  const handleClick = (e) => {
    e.preventDefault();
    updateProduct(product.uuid, dispatch, formValue);
    window.alert("product is updated");
  };

  return (
    <div className="product">
      <div className="productTitleContainer">
        <h1 className="productTitle">Product</h1>
        <Link to="/newproduct">
          <button className="productAddButton">Create</button>
        </Link>
      </div>
      <div className="productTop"></div>
      <div className="productBottom">
        <form className="productForm">
          <div className="productFormLeft">
            <label>ID {product.uuid}</label>
            <label>Product Name</label>
            <input
              type="text"
              name="title"
              placeholder={product.title}
              onChange={handleChange}
            />
            <label>Product Description</label>
            <input
              type="text"
              name="description"
              placeholder={product.description}
              onChange={handleChange}
            />
            <label>Price</label>
            <input
              type="number"
              name="price"
              placeholder={product.price}
              onChange={handleChangePrice}
            />
            <label>Categories</label>
            <input
              type="text"
              name="categories"
              placeholder={product.categories}
              onChange={handleCategory}
            />
          </div>
          <div className="productFormRight">
            <div className="productUpload">
              <img src={product.img} alt="" className="productUploadImg" />
              <label htmlFor="file">
                <Publish />
              </label>
              <input type="file" id="file" style={{ display: "none" }} />
            </div>
            <button onClick={handleClick} className="productButton">
              Update
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
