import React from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { registerSeller } from "../../redux/sellerApiCalls";
import "./registerStyle.css";
import {useHistory} from 'react-router';


const Register = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const [formValue, setFormValue] = useState({
    email: "",
    password: "",
    first_name: "",
    last_name: "",
    location: "",
    post_code: "",
    phone_number: "",
    desciption: "",
    is_admin: 0,
  });

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormValue((prevState) => {
      return {
        ...prevState,
        [name]: value,
      };
    });
  };

  const handleClick = (e) => {
    e.preventDefault();
    registerSeller(formValue, dispatch);
    window.alert("Register Succesfully");
    history.push('/login');
  };

  return (


    <div className="register_container">
      <div className="screen">
        <div className="screen__content">
          <form className="register">
            <div className="register__field">
              <i className="register__icon fas fa-user"></i>
              <input
                className="register__input"
                style={{ padding: 10, marginBottom: 20 }}
                type="email"
                name="email"
                placeholder="Email"
                onChange={handleChange}
              />
            </div>
            <div className="register__field">
              <i className="register__icon fas fa-lock"></i>
              <input
                className="register__input"
                style={{ padding: 10, marginBottom: 20 }}
                type="password"
                name="password"
                placeholder="Password"
                onChange={handleChange}
              />
            </div>
            <div className="register__field">
              <i className="register__icon fas fa-user"></i>
              <input
                className="register__input"
                style={{ padding: 10, marginBottom: 20 }}
                type="text"
                name="first_name"
                placeholder="First Name"
                onChange={handleChange}
              />
            </div>
            <div className="register__field">
              <i className="register__icon fas fa-user"></i>
              <input
                className="register__input"
                style={{ padding: 10, marginBottom: 20 }}
                type="text"
                name="last_name"
                placeholder="Last Name"
                onChange={handleChange}
              />
            </div>
            <div className="register__field">
              <i className="register__icon fas fa-user"></i>
              <input
                className="register__input"
                style={{ padding: 10, marginBottom: 20 }}
                type="text"
                name="location"
                placeholder="Location"
                onChange={handleChange}
              />
            </div>
            <div className="register__field">
              <i className="register__icon fas fa-user"></i>
              <input
                className="register__input"
                style={{ padding: 10, marginBottom: 20 }}
                type="text"
                name="post_code"
                placeholder="Post Code"
                onChange={handleChange}
              />
            </div>
            <div className="register__field">
              <i className="register__icon fas fa-user"></i>
              <input
                className="register__input"
                style={{ padding: 10, marginBottom: 20 }}
                type="text"
                name="phone_number"
                placeholder="Phone Number"
                onChange={handleChange}
              />
            </div>
            <div className="register__field">
              <i className="register__icon fas fa-user"></i>
              <input
                className="register__input"
                style={{ padding: 10, marginBottom: 20 }}
                type="text"
                name="description"
                placeholder="Description"
                onChange={handleChange}
              />
            </div>
            <button className="button register__submit" onClick={handleClick}>
              <span className="button__text">Register</span>
              <i className="button__icon fas fa-chevron-right"></i>
            </button>
          </form>

          <div className="screen__background">
            <span className="screen__background__shape screen__background__shape4"></span>
            <span className="screen__background__shape screen__background__shape3"></span>
            <span className="screen__background__shape screen__background__shape2"></span>
            <span className="screen__background__shape screen__background__shape1"></span>
          </div>

        </div>
      </div>
    </div>
  );
};

export default Register;
