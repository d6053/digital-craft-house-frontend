import React from "react";
import { PermIdentity, Publish } from "@material-ui/icons";
import "./user.css";
import { Link, useLocation } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { updateCustomer } from "../../redux/customerApiCalls";
import { useState } from "react";

export default function Customer() {
  const location = useLocation();
  const customerUuid = location.pathname.split("/")[2];
  const dispatch = useDispatch();

  const customers = useSelector((state) => state.customer.customers);
  var customer = customers.find((c) => String(c.uuid) === customerUuid);

  const [formValue, setFormValue] = useState({
    uuid: customer.uuid,
    email: customer.email,
    first_name: customer.first_name,
    last_name: customer.last_name,
  });

  const handleClick = (e) => {
    e.preventDefault();
    updateCustomer(customer.uuid, dispatch, formValue);
    window.alert("cutomer is updated");
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormValue((prevState) => {
      return {
        ...prevState,
        [name]: value,
      };
    });
  };
  return (
    <div className="user">
      <div className="userTitleContainer">
        <h1 className="userTitle">Edit Customer</h1>
        <Link to="/newCustomer">
          <button className="userAddButton">Create</button>
        </Link>
      </div>
      <div className="userContainer">
        <div className="userShow">
          <div className="userShowTop">
            <img src={customer.img} alt="" className="userShowImg" />
            <div className="userShowTopTitle">
              <span className="userShowUsername">
                {customer.first_name} {customer.last_name}
              </span>
              <span className="userShowUserTitle">Customer</span>
            </div>
          </div>
          <div className="userShowBottom">
            <span className="userShowTitle">Account Details</span>
            <div className="userShowInfo">
              <PermIdentity className="userShowIcon" />
              <span className="userShowInfoTitle">{customer.email}</span>
            </div>
          </div>
        </div>
        <div className="userUpdate">
          <span className="userUpdateTitle">Edit</span>
          <form className="userUpdateForm">
            <div className="userUpdateLeft">
              <div className="userUpdateItem">
                <div className="userUpdateItem">
                  <label>Email</label>
                  <input
                    type="email"
                    name="email"
                    placeholder={customer.email}
                    className="userUpdateInput"
                    onChange={handleChange}
                  />
                </div>
              </div>
              <div className="userUpdateItem">
                <label>First Name</label>
                <input
                  type="text"
                  name="fist_name"
                  placeholder={customer.first_name}
                  className="userUpdateInput"
                  onChange={handleChange}
                />
              </div>
              <div className="userUpdateItem">
                <label>Last Name</label>
                <input
                  type="text"
                  name="last_name"
                  placeholder={customer.last_name}
                  className="userUpdateInput"
                  onChange={handleChange}
                />
              </div>
            </div>
            <div className="userUpdateRight">
              <div className="userUpdateUpload">
                <img className="userUpdateImg" src={customer.img} alt="" />
                <label htmlFor="file">
                  <Publish className="userUpdateIcon" />
                </label>
                <input type="file" id="file" style={{ display: "none" }} />
              </div>
              <button onClick={handleClick} className="userUpdateButton">
                Update
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
