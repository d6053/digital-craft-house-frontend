import React from "react";
import {
  LocationSearching,
  PermIdentity,
  PhoneAndroid,
  Publish,
} from "@material-ui/icons";
import "./user.css";
import { Link, useLocation } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { updateSeller } from "../../redux/sellerApiCalls";
import { useState } from "react";

export default function Seller() {
  const location = useLocation();
  const sellerUuid = location.pathname.split("/")[2];
  const dispatch = useDispatch();

  const sellers = useSelector((state) => state.seller.sellers);
  var seller = sellers.find((c) => String(c.uuid) === sellerUuid);

  console.log(sellers)
  const [formValue, setFormValue] = useState({
    uuid: seller.uuid,
    email: seller.email,
    first_name: seller.first_name,
    last_name: seller.last_name,
    location: seller.location,
    post_code: seller.post_code,
    phone_number: seller.phone_number,
    desciption: seller.desciption,
  });

  const handleClick = (e) => {
    e.preventDefault();
    updateSeller(seller.uuid, dispatch, formValue);
    window.alert("seller is updated");
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormValue((prevState) => {
      return {
        ...prevState,
        [name]: value,
      };
    });
  };
  return (
    <div className="user">
      <div className="userTitleContainer">
        <h1 className="userTitle">Edit Seller</h1>
        <Link to="/newSeller">
          <button className="userAddButton">Create</button>
        </Link>
      </div>
      <div className="userContainer">
        <div className="userShow">
          <div className="userShowTop">
            <img src={seller.img} alt="" className="userShowImg" />
            <div className="userShowTopTitle">
              <span className="userShowUsername">
                {seller.first_name} {seller.last_name}
              </span>
              <span className="userShowUserTitle">Seller</span>
            </div>
          </div>
          <div className="userShowBottom">
            <span className="userShowTitle">Account Details</span>
            <div className="userShowInfo">
              <PermIdentity className="userShowIcon" />
              <span className="userShowInfoTitle">{seller.email}</span>
            </div>

            <div className="userShowInfo">
              <PhoneAndroid className="userShowIcon" />
              <span className="userShowInfoTitle">{seller.phone_number}</span>
            </div>

            <div className="userShowInfo">
              <LocationSearching className="userShowIcon" />
              <span className="userShowInfoTitle">
                {seller.location} {seller.post_code}
              </span>
            </div>

            <div className="userShowInfo">
              <span className="userShowInfoTitle">{seller.description}</span>
            </div>
          </div>
        </div>
        <div className="userUpdate">
          <span className="userUpdateTitle">Edit</span>
          <form className="userUpdateForm">
            <div className="userUpdateLeft">
              <div className="userUpdateItem">
                <div className="userUpdateItem">
                  <label>Email</label>
                  <input
                    type="email"
                    name="email"
                    placeholder={seller.email}
                    className="userUpdateInput"
                    onChange={handleChange}
                  />
                </div>
              </div>
              <div className="userUpdateItem">
                <label>First Name</label>
                <input
                  type="text"
                  name="first_name"
                  placeholder={seller.first_name}
                  className="userUpdateInput"
                  onChange={handleChange}
                />
              </div>
              <div className="userUpdateItem">
                <label>Last Name</label>
                <input
                  type="text"
                  name="last_name"
                  placeholder={seller.last_name}
                  className="userUpdateInput"
                  onChange={handleChange}
                />
              </div>

              <div className="userUpdateItem">
                <label>Phone Number</label>
                <input
                  type="text"
                  name="phone_numeber"
                  placeholder={seller.phone_number}
                  className="userUpdateInput"
                  onChange={handleChange}
                />
              </div>

              <div className="userUpdateItem">
                <label>Location</label>
                <input
                  type="text"
                  name="location"
                  placeholder={seller.location}
                  className="userUpdateInput"
                  onChange={handleChange}
                />
              </div>

              <div className="userUpdateItem">
                <label>Post Code</label>
                <input
                  type="text"
                  name="post_code"
                  placeholder={seller.post_code}
                  className="userUpdateInput"
                  onChange={handleChange}
                />
              </div>
            </div>
            <div className="userUpdateRight">
              <div className="userUpdateUpload">
                <img className="userUpdateImg" src={seller.img} alt="" />
                <label htmlFor="file">
                  <Publish className="userUpdateIcon" />
                </label>
                <input type="file" id="file" style={{ display: "none" }} />
              </div>
              <button onClick={handleClick} className="userUpdateButton">
                Update
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
