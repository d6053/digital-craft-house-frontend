import React from "react";
import "./userList.css";
import { DataGrid } from "@material-ui/data-grid";
import { DeleteOutline } from "@material-ui/icons";
import { Link } from "react-router-dom";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteCustomer, getCustomers } from "../../redux/customerApiCalls";

export default function CustomerList() {
  const dispatch = useDispatch();
  const customers = useSelector((state) => state.customer.customers);

  useEffect(() => {
    getCustomers(dispatch);
  }, [dispatch]);

  const handleDelete = (uuid) => {
    deleteCustomer(uuid, dispatch);
  };

  const columns = [
    { field: "uuid", headerName: "ID", width: 90 },
    {
      field: "customer",
      headerName: "Customer",
      width: 200,
      renderCell: (params) => {
        return (
          <div className="userListUser">
            <img className="userListImg" src={params.row.avatar} alt="" />
            {params.row.username}
          </div>
        );
      },
    },
    { field: "email", headerName: "Email", width: 200 },
    {
      field: "status",
      headerName: "Status",
      width: 120,
    },
    {
      field: "transaction",
      headerName: "Transaction Volume",
      width: 160,
    },
    {
      field: "action",
      headerName: "Action",
      width: 150,
      renderCell: (params) => {
        return (
          <>
            <Link to={"/customers/" + params.row.uuid}>
              <button className="userListEdit">Edit</button>
            </Link>
            <DeleteOutline
              className="userListDelete"
              onClick={() => handleDelete(params.row.uuid)}
            />
          </>
        );
      },
    },
  ];

  return (
    <div className="userList">
      <Link to="/newCustomer">
        <button className="userAddButton">Create</button>
      </Link>
      <DataGrid
        rows={customers}
        disableSelectionOnClick
        columns={columns}
        getRowId={(row) => row.uuid}
        pageSize={8}
        checkboxSelection
      />
    </div>
  );
}
