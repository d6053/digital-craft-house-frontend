import { userRequest} from "../requestMethods";
import {
  getCustomerFailure,
  getCustomerStart,
  getCustomerSuccess,
  deleteCustomerFailure,
  deleteCustomerStart,
  deleteCustomerSuccess,
  updateCustomerFailure,
  updateCustomerStart,
  updateCustomerSuccess,
  addCustomerFailure,
  addCustomerStart,
  addCustomerSuccess,
} from "./customerRedux";


export const getCustomers = async (dispatch) => {
  dispatch(getCustomerStart());
  try {
    const res = await userRequest.get("/customers",{
      withCredentials : true
    });
    dispatch(getCustomerSuccess(res.data));
  } catch (err) {
    dispatch(getCustomerFailure());
  }
};

export const deleteCustomer = async (uuid, dispatch) => {
  dispatch(deleteCustomerStart());
  try {
    const res = await userRequest.delete(`/customers/${uuid}`,
    {
      withCredentials : true
    });
    dispatch(deleteCustomerSuccess(uuid));
  } catch (err) {
    dispatch(deleteCustomerFailure());
  }
};

export const updateCustomer = async (uuid, dispatch, customer) => {
  dispatch(updateCustomerStart());
  try {
    const res = await userRequest.put(
      `/customers/${uuid}`,
      customer,
      {
        withCredentials : true
      }
    );
    dispatch(updateCustomerSuccess({ uuid, customer }));
  } catch (err) {
    dispatch(updateCustomerFailure());
  }
};
export const addCustomer = async (customer, dispatch) => {
  dispatch(addCustomerStart());
  try {
    const res = await userRequest.post(`/customers`, customer,
    {
      withCredentials : true
    });
    dispatch(addCustomerSuccess(res.data));
  } catch (err) {
    dispatch(addCustomerFailure());
    console.log(err);
  }
};
