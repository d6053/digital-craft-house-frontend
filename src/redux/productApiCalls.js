import { authRequest, productRequest } from "../requestMethods";
import {
  getProductFailure,
  getProductStart,
  getProductSuccess,
  deleteProductFailure,
  deleteProductStart,
  deleteProductSuccess,
  updateProductFailure,
  updateProductStart,
  updateProductSuccess,
  addProductFailure,
  addProductStart,
  addProductSuccess,
} from "./productRedux";
import axios from "axios";


export const getProducts = async (dispatch) => {
  dispatch(getProductStart());
  try {
    const res = await productRequest.get("/products");
    dispatch(getProductSuccess(res.data));
  } catch (err) {
    dispatch(getProductFailure());
  }
};

export const deleteProduct = async (uuid, dispatch) => {
  dispatch(deleteProductStart());
  try {
    const res = await productRequest
      .delete(`/products/${uuid}`)
      .then(function(response) {
        console.log(response);
      })
      .catch(function(error) {
        console.log(error);
      });
    dispatch(deleteProductSuccess(uuid));
  } catch (err) {
    dispatch(deleteProductFailure());
  }
};

export const updateProduct = async (uuid, dispatch, product) => {
  dispatch(updateProductStart());
  try {
    //
    const res = await productRequest.put(
      `/products/${uuid}`,
      product
    );
    dispatch(updateProductSuccess({ uuid, product }));
  } catch (err) {
    dispatch(updateProductFailure());
    console.log(err);
  }
};

export const addProduct = async (product, dispatch) => {
  dispatch(addProductStart());
  try {
    const res = await productRequest.post(`/products`, product);
    dispatch(addProductSuccess(res.data));
  } catch (err) {
    dispatch(addProductFailure());
    console.log(err);
  }
};
