import { loginFailure, loginStart, loginSuccess, logout } from "./userRedux";
import { publicRequest, userRequest } from "../requestMethods";
import {
  getSellerFailure,
  getSellersStart,
  getSellerSuccess,
  deleteSellerFailure,
  deleteSellerStart,
  deleteSellerSuccess,
  updateSellerFailure,
  updateSellerStart,
  updateSellerSuccess,
  addSellerFailure,
  addSellerStart,
  addSellerSuccess,
} from "./sellerRedux";
import { authRequest } from "../requestMethods";

export const login = async (user, dispatch) => {
  dispatch(loginStart());
  try {
    const res = await authRequest.post(`/auth/login`, user, {
      withCredentials : true
    });
    dispatch(loginSuccess(res.data));
  } catch (err) {
    dispatch(loginFailure());
    console.log(err);
  }
};

export const logoutFunc = async (dispatch) => {
  dispatch(logout());
  try {
    const res = await authRequest.post(`/auth/logout`, null, {
      withCredentials : true
    });
    window.alert(res.data);
  } catch (err) {
    console.log(err);
  }
};

export const getSellers = async (dispatch) => {
  dispatch(getSellersStart());
  try {
    const res = await userRequest.get("/sellers", {
      withCredentials : true
    });
    dispatch(getSellerSuccess(res.data));
  } catch (err) {
    dispatch(getSellerFailure());
  }
};

export const deleteSeller = async (uuid, dispatch) => {
  dispatch(deleteSellerStart());
  try {
    const res = await userRequest.delete(`/sellers/${uuid}`,{
      withCredentials : true
    });
    dispatch(deleteSellerSuccess(uuid));
  } catch (err) {
    dispatch(deleteSellerFailure());
  }
};

export const updateSeller = async (uuid, dispatch, seller) => {
  dispatch(updateSellerStart());
  try {
    const res = await userRequest.put(
      `/sellers/${uuid}`,
      seller,
      {
        withCredentials : true
      }
    );
    dispatch(updateSellerSuccess({ uuid, seller }));
  } catch (err) {
    dispatch(updateSellerFailure());
  }
};
export const addSeller = async (seller, dispatch) => {
  dispatch(addSellerStart());
  try {
    const res = await userRequest.post(`/sellers`, seller,
    {
      withCredentials : true
    });
    dispatch(addSellerSuccess(res.data));
  } catch (err) {
    dispatch(addSellerFailure());
  }
};

export const registerSeller = async (seller, dispatch) => {
  dispatch(addSellerStart());
  try {
    const res = await authRequest.post(`/auth/register`, seller,
    {
      withCredentials : true
    });
    dispatch(addSellerSuccess(res.data));
  } catch (err) {
    dispatch(addSellerFailure());
  }
};
