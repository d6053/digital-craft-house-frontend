import { createSlice } from "@reduxjs/toolkit";

export const sellerSlice = createSlice({
  name: "seller",
  initialState: {
    sellers: [],
    isFetching: false,
    error: false,
  },
  reducers: {
    //GET ALL
    getSellersStart: (state) => {
      state.isFetching = true;
      state.error = false;
    },
    getSellerSuccess: (state, action) => {
      state.isFetching = false;
      state.sellers = action.payload;
    },
    getSellerFailure: (state) => {
      state.isFetching = false;
      state.error = true;
    },
    //DELETE
    deleteSellerStart: (state) => {
      state.isFetching = true;
      state.error = false;
    },
    deleteSellerSuccess: (state, action) => {
      state.isFetching = false;
      state.sellers.splice(
        state.sellers.findIndex((item) => item.uuid === action.payload),
        1
      );
    },
    deleteSellerFailure: (state) => {
      state.isFetching = false;
      state.error = true;
    },
    //UPDATE
    updateSellerStart: (state) => {
      state.isFetching = true;
      state.error = false;
    },
    updateSellerSuccess: (state, action) => {
      state.isFetching = false;
      state.sellers[
        state.sellers.findIndex((item) => item.uuid === action.payload.uuid)
      ] = action.payload.seller;
    },
    updateSellerFailure: (state) => {
      state.isFetching = false;
      state.error = true;
    },
    //UPDATE
    addSellerStart: (state) => {
      state.isFetching = true;
      state.error = false;
    },
    addSellerSuccess: (state, action) => {
      state.isFetching = false;
      state.sellers.push(action.payload);
    },
    addSellerFailure: (state) => {
      state.isFetching = false;
      state.error = true;
    },
  },
});

export const {
  getSellersStart,
  getSellerSuccess,
  getSellerFailure,
  deleteSellerStart,
  deleteSellerSuccess,
  deleteSellerFailure,
  updateSellerStart,
  updateSellerSuccess,
  updateSellerFailure,
  addSellerStart,
  addSellerSuccess,
  addSellerFailure,
} = sellerSlice.actions;

export default sellerSlice.reducer;
