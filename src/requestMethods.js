import axios from "axios";

const USER_URL = `http://localhost:9000`;
const AUTH_URL = `http://localhost:9006`;
const PRODUCT_URL = `http://localhost:9005`;

export const userRequest = axios.create({
  baseURL: USER_URL,
});

export const authRequest = axios.create({
  baseURL: AUTH_URL,
});

export const productRequest = axios.create({
  baseURL: PRODUCT_URL,
});
